package com.example.login;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {


    //// Widgets
    public EditText FNAME;
    public EditText LNAME;
    public EditText EMAIL;
    public EditText PASSWORD;
    public ImageView TOGGLE_PASSWORD;
    public EditText CONFIRM_PASSWORD;
    public ImageView TOGGLE_CONFIRM_PASSWORD;
    public Button REGISTER;
    public TextView LOGIN;


    //// Database Variables
    public DBHelper DB_HELPER;
    public SQLiteDatabase DB_CONNECTION;


    //// Other Variables
    public Toast userExistsToast;
    public Toast exitToast;
    public boolean backPressed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        //// Set Static variables to respective views
        FNAME = findViewById(R.id.fname);
        LNAME = findViewById(R.id.lname);
        EMAIL = findViewById(R.id.email);
        PASSWORD = findViewById(R.id.password);
        CONFIRM_PASSWORD = findViewById(R.id.confirmPassowrd);
        TOGGLE_PASSWORD = findViewById(R.id.togglePassword);
        TOGGLE_CONFIRM_PASSWORD = findViewById(R.id.toggleConfirmPassword);
        REGISTER = findViewById(R.id.register);
        LOGIN = findViewById(R.id.login);


        userExistsToast = Toast.makeText(RegisterActivity.this, "Given Email ID is already registered", Toast.LENGTH_LONG);
        exitToast = Toast.makeText(RegisterActivity.this, "Press back again to exit", Toast.LENGTH_SHORT);


        DB_HELPER = new DBHelper(this);
        DB_CONNECTION = DB_HELPER.getWritableDatabase();


        //// Toggle Password
        TOGGLE_PASSWORD.setOnClickListener(new PasswordToggleListener(PASSWORD, TOGGLE_PASSWORD));

        //// Toggle Confirm Password
        TOGGLE_CONFIRM_PASSWORD.setOnClickListener(new PasswordToggleListener(CONFIRM_PASSWORD, TOGGLE_CONFIRM_PASSWORD));


        //// Validate Input Fields
        REGISTER.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean valid = true;


                //// Check First Name
                if (!FNAME.getText().toString().matches("[a-zA-Z]+")) {
                    FNAME.setError("Invalid. First Name can contain only A-Z a-z");
                    valid = false;
                }


                //// Check Last Name
                if (!LNAME.getText().toString().matches("[a-zA-Z]+")) {
                    LNAME.setError("Invalid. Last Name can contain only A-Z a-z");
                    valid = false;
                }


                //// Check Email
                if (!EMAIL.getText().toString().matches("[a-zA-Z0-9.]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+")) {
                    EMAIL.setError("Invalid Email");
                    valid = false;
                }


                //// Check Password
                if (PASSWORD.getText().toString().length() < 8 || PASSWORD.getText().toString().length() > 15) {
                    PASSWORD.setError("Password length should be between 8 - 15 characters");
                    valid = false;
                }


                //// Check Confirm Password
                if (!CONFIRM_PASSWORD.getText().toString().equals(PASSWORD.getText().toString())) {
                    CONFIRM_PASSWORD.setError("Password does not match.");
                    valid = false;
                }


                //// If All input fields are valid
                if (valid) {

                    try {

                        //// Insert Data into Database
                        long result = DB_HELPER.insertData(DB_CONNECTION, FNAME.getText().toString(), LNAME.getText().toString(), EMAIL.getText().toString(), PASSWORD.getText().toString());

                        if (result == -1)
                            throw new SQLiteConstraintException("User Exists");

                        Toast.makeText(RegisterActivity.this, "Successfully Registered", Toast.LENGTH_SHORT).show();


                        //// Go to Login Activity
                        Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(i);

                    } catch (SQLiteConstraintException e) {
                        userExistsToast.show();
                    }
                }

            }
        });

    }


    //// Toggle Password and Confirm Password
    public class PasswordToggleListener implements View.OnClickListener {

        private EditText PASSWORD;
        private ImageView TOGGLE_BUTTON;

        private boolean passwordVisible = false;

        PasswordToggleListener(EditText pass, ImageView toggle) {

            PASSWORD = pass;
            TOGGLE_BUTTON = toggle;

        }

        public void onClick(View v) {
            if (passwordVisible) {
                TOGGLE_BUTTON.setImageResource(R.drawable.make_password_visible);
                PASSWORD.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                passwordVisible = false;
            } else {
                TOGGLE_BUTTON.setImageResource(R.drawable.make_password_invisible);
                PASSWORD.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                passwordVisible = true;
            }
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        userExistsToast.cancel();
        exitToast.cancel();
    }


    public void onLogin(View v) {
        Intent i = new Intent(this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }


    @Override
    public void onBackPressed() {

        if (backPressed) {
            super.onBackPressed();
        }

        backPressed = true;
        exitToast.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                backPressed = false;
            }
        }, 2000);
    }
}
