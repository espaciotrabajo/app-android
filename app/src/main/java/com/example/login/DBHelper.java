package com.example.login;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static String DB_NAME = "users";
    public static String TABLE_NAME = "users";
    public static int DB_VERSION = 1;
    public static String COLUMN__ID = "_id";
    public static String COLUMN_FIRST_NAME = "first_name";
    public static String COLUMN_LAST_NAME = "last_name";
    public static String COLUMN_EMAIL = "email";
    public static String COLUMN_PASSWORD = "password";


    DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_FIRST_NAME + " TEXT, " +
                COLUMN_LAST_NAME + " TEXT, " +
                COLUMN_EMAIL + " TEXT UNIQUE NOT NULL, " +
                COLUMN_PASSWORD + " TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public long insertData(SQLiteDatabase db, String firstName, String lastName, String email, String password) {

        ContentValues data = new ContentValues();
        data.put(COLUMN_FIRST_NAME, firstName);
        data.put(COLUMN_LAST_NAME, lastName);
        data.put(COLUMN_EMAIL, email);
        data.put(COLUMN_PASSWORD, password);

        return db.insert(TABLE_NAME, null, data);
    }

    public int updateData(SQLiteDatabase db, int id, String firstName, String lastName, String email, String password) {

        ContentValues data = new ContentValues();
        data.put(COLUMN_FIRST_NAME, firstName);
        data.put(COLUMN_LAST_NAME, lastName);
        data.put(COLUMN_EMAIL, email);
        data.put(COLUMN_PASSWORD, password);

        return db.update(TABLE_NAME, data, COLUMN__ID + " = ?", new String[]{String.valueOf(id)});
    }
}
