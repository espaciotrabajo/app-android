package com.example.login;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.swipeactionadapter.SwipeActionAdapter;
import com.wdullaer.swipeactionadapter.SwipeDirection;


public class AllUsersFragment extends Fragment {


    //// Widgets
    public ListView ALL_USERS_LIST_VIEW;
    public ImageView SEARCH_BUTTON;
    public EditText SEARCH_BAR;


    //// Database Variables
    public DBHelper DB_HELPER;
    public SQLiteDatabase DB_CONNECTION;
    public Cursor CURSOR;
    public CursorAdapter CURSOR_ADAPTER;
    public SwipeActionAdapter SWIPE_ADAPTER;


    //// Other Variables
    private Context context;
    private int userId;


    public AllUsersFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void setUserId(int id) {
        userId = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_all_users, container, false);

        ALL_USERS_LIST_VIEW = v.findViewById(R.id.allUsersListView);
        SEARCH_BUTTON = v.findViewById(R.id.searchButton);
        SEARCH_BAR = v.findViewById(R.id.searchInput);


        DB_HELPER = new DBHelper(context);
        DB_CONNECTION = DB_HELPER.getReadableDatabase();


        if (savedInstanceState != null) {
            userId = savedInstanceState.getInt("userId");
        }


        try {


            //// Get Cursor to database for all Data
            CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME,
                    new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_LAST_NAME, DBHelper.COLUMN_EMAIL},
                    "NOT " + DBHelper.COLUMN__ID + " = ?", new String[]{String.valueOf(userId)},
                    null, null, DBHelper.COLUMN_FIRST_NAME);


            //// Set cursor adapter to display data in drawer list
            CURSOR_ADAPTER = new SimpleCursorAdapter(context, R.layout.all_users_list_view, CURSOR,
                    new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_LAST_NAME, DBHelper.COLUMN_EMAIL},
                    new int[]{R.id.id, R.id.fname, R.id.lname, R.id.email}, 0);
            SWIPE_ADAPTER = new SwipeActionAdapter(CURSOR_ADAPTER);
            SWIPE_ADAPTER.setListView(ALL_USERS_LIST_VIEW);
            ALL_USERS_LIST_VIEW.setAdapter(SWIPE_ADAPTER);


            SWIPE_ADAPTER.addBackground(SwipeDirection.DIRECTION_NORMAL_RIGHT, R.layout.right_swipe)
                    .addBackground(SwipeDirection.DIRECTION_NORMAL_LEFT, R.layout.left_swipe);

            SWIPE_ADAPTER.setSwipeActionListener(new SwipeActionAdapter.SwipeActionListener() {
                @Override
                public boolean hasActions(int position, SwipeDirection direction) {
                    if (direction.isLeft()) return true;
                    if (direction.isRight()) return true;
                    return false;
                }

                @Override
                public boolean shouldDismiss(int position, SwipeDirection direction) {
                    return false;
                }

                @Override
                public void onSwipe(final int[] position, SwipeDirection[] direction) {

                    for (int i = 0; i < position.length; i++) {

                        SwipeDirection swipeDirection = direction[i];
                        final int pos = position[i];

                        switch (swipeDirection) {
                            case DIRECTION_FAR_LEFT:
                            case DIRECTION_NORMAL_LEFT:

                                Cursor c = (Cursor) SWIPE_ADAPTER.getItem(pos);
                                int editUserId = c.getInt(c.getColumnIndex(DBHelper.COLUMN__ID));
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                Fragment fragment = new UpdateFragment();
                                ((UpdateFragment) fragment).setUserId(editUserId);
                                transaction.replace(R.id.frameLayout, fragment, "fragment");
                                transaction.addToBackStack(null);
                                transaction.commit();

                                break;

                            case DIRECTION_FAR_RIGHT:
                            case DIRECTION_NORMAL_RIGHT:

                                AlertDialog.Builder delete = new AlertDialog.Builder(context);
                                delete.setTitle("Delete User");
                                delete.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Cursor c = (Cursor) SWIPE_ADAPTER.getItem(pos);
                                        String deleteUserId = c.getString(c.getColumnIndex(DBHelper.COLUMN__ID));
                                        DB_CONNECTION.delete(DBHelper.TABLE_NAME, DBHelper.COLUMN__ID + " = ?", new String[]{deleteUserId});

                                        String searchInput = SEARCH_BAR.getText().toString();
                                        if (searchInput.equals("")) {
                                            CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME,
                                                    new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_LAST_NAME, DBHelper.COLUMN_EMAIL},
                                                    "NOT " + DBHelper.COLUMN__ID + " = ?", new String[]{String.valueOf(userId)},
                                                    null, null, DBHelper.COLUMN_FIRST_NAME);
                                        } else {
                                            String condition = "( NOT " + DBHelper.COLUMN__ID + " = " + userId + " ) AND ( " +
                                                    DBHelper.COLUMN_FIRST_NAME + " like \"%" + searchInput + "%\" OR " +
                                                    DBHelper.COLUMN_LAST_NAME + " like \"%" + searchInput + "%\" OR " +
                                                    DBHelper.COLUMN_EMAIL + " like \"%" + searchInput + "%\" )";

                                            CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME,
                                                    new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_LAST_NAME, DBHelper.COLUMN_EMAIL},
                                                    condition, null, null, null, DBHelper.COLUMN_FIRST_NAME);
                                        }

                                        CURSOR_ADAPTER.changeCursor(CURSOR);
                                        Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                delete.setNegativeButton("Cancel", null);
                                delete.show();
                                break;
                        }

                    }
                }
            });


            //// Show data on selected user
            AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    DetailFragment fragment = new DetailFragment();

                    //// Pass email to DetailFragment
                    int sendUserId = Integer.parseInt(((TextView) view.findViewById(R.id.id)).getText().toString());

                    fragment.setUserId(sendUserId);
                    transaction.replace(R.id.frameLayout, fragment, "fragment");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            };
            ALL_USERS_LIST_VIEW.setOnItemClickListener(listener);

        } catch (SQLException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }


        SEARCH_BUTTON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchUsers(SEARCH_BAR.getText().toString());
            }
        });

        SEARCH_BAR.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchUsers(String.valueOf(s));
            }
        });


        return v;
    }


    public void searchUsers(String input) {

        String condition = "( NOT " + DBHelper.COLUMN__ID + " = " + userId + " ) AND ( " +
                DBHelper.COLUMN_FIRST_NAME + " like \"%" + input + "%\" OR " +
                DBHelper.COLUMN_LAST_NAME + " like \"%" + input + "%\" OR " +
                DBHelper.COLUMN_EMAIL + " like \"%" + input + "%\" )";

        CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME,
                new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_LAST_NAME, DBHelper.COLUMN_EMAIL},
                condition, null, null, null, DBHelper.COLUMN_FIRST_NAME);

        CURSOR_ADAPTER.changeCursor(CURSOR);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("userId", userId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CURSOR.close();
        DB_CONNECTION.close();
    }
}
