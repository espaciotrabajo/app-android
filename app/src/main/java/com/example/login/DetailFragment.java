package com.example.login;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetailFragment extends Fragment {


    //// Widgets
    private TextView HEADING;
    private TextView NAME;
    private TextView EMAIL;
    private TextView PASSWORD;
    private Button EDIT;
    private Button DELETE;


    //// Database Variable
    public DBHelper DB_HELPER;
    public SQLiteDatabase DB_CONNECTION;
    public Cursor CURSOR;


    //// Other Variables
    private int userId;
    private Context context;
    private boolean personalDetails = false;


    public DetailFragment() {
    }

    public void setUserId(int id) {
        this.userId = id;
    }

    public void setPersonal(boolean personal) {
        personalDetails = personal;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_detail, container, false);

        HEADING = v.findViewById(R.id.heading);
        NAME = v.findViewById(R.id.name);
        EMAIL = v.findViewById(R.id.email);
        PASSWORD = v.findViewById(R.id.password);
        EDIT = v.findViewById(R.id.editButton);
        DELETE = v.findViewById(R.id.deleteButton);

        if( savedInstanceState != null ){
            userId = savedInstanceState.getInt("userId");
            personalDetails = savedInstanceState.getBoolean("personalDetails");
        }


        //// Perform Database Operations
        DB_HELPER = new DBHelper(context);
        DB_CONNECTION = DB_HELPER.getReadableDatabase();
        CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME,
                new String[]{DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_LAST_NAME, DBHelper.COLUMN_EMAIL, DBHelper.COLUMN_PASSWORD},
                DBHelper.COLUMN__ID + " = ?", new String[]{String.valueOf(userId)},
                null, null, null);


        if (CURSOR.moveToFirst()) {

            NAME.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_FIRST_NAME)) + " " + CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_LAST_NAME)));
            EMAIL.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_EMAIL)));
            PASSWORD.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_PASSWORD)));

        }


        if (!personalDetails) {
            HEADING.setVisibility(View.INVISIBLE);
        }


        //// Delete click listener
        DELETE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DB_CONNECTION.delete(DBHelper.TABLE_NAME, DBHelper.COLUMN__ID + " = ?", new String[]{String.valueOf(userId)});
                        Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();

                        //// If user deleted personal account, Log Out
                        //// Else go back to previous fragment
                        if (personalDetails) {

                            //// Clear shared preference
                            SharedPreferences.Editor editor = context.getSharedPreferences("userData", Activity.MODE_PRIVATE).edit();
                            editor.clear();
                            editor.apply();

                            Intent i = new Intent(context, LoginActivity.class);
                            startActivity(i);

                        } else {
                            getFragmentManager().popBackStack();
                        }
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.setTitle("Delete User...");
                alertDialog.show();

            }
        });

        EDIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Fragment fragment = new UpdateFragment();
                ((UpdateFragment) fragment).setUserId(userId);
                transaction.replace(R.id.frameLayout, fragment, "fragment");
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("userId", userId);
        outState.putBoolean("personalDetails", personalDetails);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CURSOR.close();
        DB_CONNECTION.close();
    }

    public int getUserId(){
        return userId;
    }

}
