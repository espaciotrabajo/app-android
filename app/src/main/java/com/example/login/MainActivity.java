package com.example.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    //// Widgets
    public ImageView DETAIL_BUTTON;
    public ImageView LIST_BUTTON;
    public RelativeLayout COVER;
    public TextView COVER_NAME;
    public TextView COVER_EMAIL;


    //// Drawer Variables
    public ListView DRAWER_LIST_VIEW;
    public LinearLayout DRAWER_LINEAR_LAYOUT;
    public DrawerLayout DRAWER_LAYOUT;
    public ActionBarDrawerToggle DRAWER_TOGGLE;
    public ArrayAdapter<String> DRAWER_ARRAY_ADAPTER;


    //// Database Variables
    public DBHelper DB_HELPER;
    public SQLiteDatabase DB_CONNECTION;
    public Cursor CURSOR;


    //// Other Variables
    public boolean backPressed = false;
    public Toast backPressedToast;
    public int currentOptionPosition = -1;
    public int userId;


    public String generateRandomColor() {

        String hexValues = "0123456789ABCDEF";
        String color = "#";
        for (int i = 0; i < 6; i++) {
            color += hexValues.charAt((int) (Math.random() * hexValues.length()));
        }
        return color;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        backPressedToast = Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT);


        DB_HELPER = new DBHelper(this);
        DB_CONNECTION = DB_HELPER.getReadableDatabase();


        DETAIL_BUTTON = findViewById(R.id.detailButton);
        LIST_BUTTON = findViewById(R.id.listButton);
        COVER = findViewById(R.id.cover);
        COVER_NAME = findViewById(R.id.coverName);
        COVER_EMAIL = findViewById(R.id.coverEmail);


        //// Get userId passed from Login Activity
        if (savedInstanceState != null) {
            userId = savedInstanceState.getInt("userId");
        } else {
            userId = getIntent().getIntExtra("userId", -1);
        }


        //// Get name of Logged in user
        CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME,
                new String[]{DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_EMAIL},
                DBHelper.COLUMN__ID + " = ?", new String[]{String.valueOf(userId)},
                null, null, null);
        CURSOR.moveToFirst();


        //// Change Cover Values
        COVER.setBackgroundColor(Color.parseColor(generateRandomColor()));
        COVER_NAME.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_FIRST_NAME)));
        COVER_EMAIL.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_EMAIL)));


        //// Set up List View, Drawer toggle, Drawer Listener
        setupDrawer();


        detailButton(null);


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                Fragment visibleFragment = getSupportFragmentManager().findFragmentByTag("fragment");

                if (visibleFragment instanceof AllUsersFragment) {
                    currentOptionPosition = 0;
                } else if (visibleFragment instanceof DetailFragment) {
                    if (((DetailFragment) visibleFragment).getUserId() == userId) {
                        currentOptionPosition = 1;
                    } else {
                        currentOptionPosition = 3;
                    }
                } else if (visibleFragment instanceof AboutFragment) {
                    currentOptionPosition = 2;
                }
                DRAWER_ARRAY_ADAPTER.notifyDataSetChanged();
                toggleButtons(currentOptionPosition);

            }
        });


        //// Show & Hide Drawer
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                DRAWER_LAYOUT.openDrawer(Gravity.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DRAWER_LAYOUT.closeDrawer(Gravity.START);
                    }
                }, 700);
            }
        }, 400);

    }


    //// Set up List View, Drawer toggle, Drawer Listener
    public void setupDrawer() {

        String[] drawerOption = getResources().getStringArray(R.array.actionBarMenu);


        //// Set static variables to their respective view
        DRAWER_LIST_VIEW = findViewById(R.id.listView);
        DRAWER_LAYOUT = findViewById(R.id.drawerLayout);
        DRAWER_LINEAR_LAYOUT = findViewById(R.id.linearLayout);


        //// Set data to drawer list view
        DRAWER_ARRAY_ADAPTER = new ArrayAdapter<String>(this, R.layout.action_bar_list_view, R.id.textView, drawerOption) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);

                if (currentOptionPosition == position + 2) {
                    v.setBackgroundColor(Color.parseColor("#cccccc"));
                } else {
                    v.setBackgroundColor(Color.parseColor("#ffffff"));
                }

                return v;
            }
        };
        DRAWER_LIST_VIEW.setAdapter(DRAWER_ARRAY_ADAPTER);


        //// Set Drawer Toggle Bar
        DRAWER_TOGGLE = new ActionBarDrawerToggle(this, DRAWER_LAYOUT, R.string.drawerOpen, R.string.drawerClose) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                //// Change Cover Background Color
                COVER.setBackgroundColor(Color.parseColor(generateRandomColor()));
            }
        };
        DRAWER_LAYOUT.setDrawerListener(DRAWER_TOGGLE);


        //// Set Drawer List CLick Listener
        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (currentOptionPosition == position + 2)
                    return;

                Fragment fragment = new Fragment();
                switch (position) {
                    case 0:
                        fragment = new AboutFragment();
                        break;
                }

                changeFragment(fragment);
                DRAWER_LAYOUT.closeDrawer(DRAWER_LINEAR_LAYOUT);
            }
        };
        DRAWER_LIST_VIEW.setOnItemClickListener(listener);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        DRAWER_TOGGLE.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (DRAWER_TOGGLE.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    public void onLogout(View v) {


        //// Remove all userId from shared preference
        SharedPreferences.Editor editor = getSharedPreferences("userData", Activity.MODE_PRIVATE).edit();
        editor.putInt("userId", -1);
        editor.apply();

        Intent i = new Intent(this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        backPressedToast.cancel();
    }

    @Override
    public void onBackPressed() {

        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();

        if (DRAWER_LAYOUT.isDrawerOpen(DRAWER_LINEAR_LAYOUT)) {
            DRAWER_LAYOUT.closeDrawer(DRAWER_LINEAR_LAYOUT);
            return;
        }

        if (backStackCount > 1) {
            super.onBackPressed();
        } else {

            if( backPressed ){
                finish();
            }

            backPressedToast.show();
            backPressed = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressed = false;
                }
            }, 2000);

        }
    }

    public void changeFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment, "fragment");
        transaction.addToBackStack(null);
        transaction.commit();

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("userId", userId);
    }

    public void detailButton(View v) {
        if (currentOptionPosition != 1) {
            currentOptionPosition = 1;
            DetailFragment fragment = new DetailFragment();
            fragment.setPersonal(true);
            fragment.setUserId(userId);
            changeFragment(fragment);
        }
    }

    public void listButton(View v) {
        if (currentOptionPosition != 0) {
            currentOptionPosition = 0;
            AllUsersFragment fragment = new AllUsersFragment();
            fragment.setUserId(userId);
            changeFragment(fragment);
        }
    }

    public void toggleButtons(int pos) {
        if (pos == 0) {
            LIST_BUTTON.setImageResource(R.drawable.ic_list_white_24dp);
            DETAIL_BUTTON.setImageResource(R.drawable.ic_person_grey_24dp);
        } else if (pos == 1) {
            LIST_BUTTON.setImageResource(R.drawable.ic_list_grey_24dp);
            DETAIL_BUTTON.setImageResource(R.drawable.ic_person_white_24dp);
        } else {
            LIST_BUTTON.setImageResource(R.drawable.ic_list_grey_24dp);
            DETAIL_BUTTON.setImageResource(R.drawable.ic_person_grey_24dp);
        }

    }
}
