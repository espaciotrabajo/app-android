package com.example.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {


    //// Widgets
    public AutoCompleteTextView EMAIL;
    public EditText PASSWORD;
    public ImageView TOGGLE_PASSWORD;
    public Button LOGIN;
    public TextView REGISTER;
    public CheckBox REMEMBER_ME;

    //// Database Variables
    public DBHelper DB_HELPER;
    public SQLiteDatabase DB_CONNECTION;
    public Cursor CURSOR;

    public SQLiteDatabase suggestionConnection;
    public Cursor suggestionCursor;
    public CursorAdapter suggestionCursorAdapter;


    //// Other Variables
    public boolean passwordVisible = false;
    public boolean backPressed = false;
    public Toast invalidUserToast;
    public Toast exitToast;
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor sharedPreferenceEditor;
    public int userId;


    public void AddDummyUsers() {

        String charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        DBHelper dummyHelper = new DBHelper(this);
        SQLiteDatabase dummyDB = dummyHelper.getWritableDatabase();

        for (int users = 0; users < 50; users++) {

            StringBuffer temp = new StringBuffer();

            for (int i = 0; i < 5; i++) {
                temp.append(charSet.charAt((int) (Math.random() * charSet.length())));
            }

            String dummyFirstName = String.valueOf(temp);
            dummyHelper.insertData(dummyDB, dummyFirstName, "User", dummyFirstName + "@" + users + ".com", "12345678");

        }
        dummyDB.close();
        Toast.makeText(this, "Inserted Dummy Users", Toast.LENGTH_SHORT).show();
    }

    public void DeleteDummyUsers() {

        DBHelper dummyHelper = new DBHelper(this);
        SQLiteDatabase dummyDB = dummyHelper.getWritableDatabase();

        for (int users = 0; users < 50; users++) {
            dummyDB.delete(DBHelper.TABLE_NAME, DBHelper.COLUMN_LAST_NAME + " = ?", new String[]{"User"});
        }
        dummyDB.close();
        Toast.makeText(this, "Deleted Dummy Users", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //AddDummyUsers();
        //DeleteDummyUsers();
        //new ExtendAsyncTask().execute(1);

        invalidUserToast = Toast.makeText(this, "Invalid User", Toast.LENGTH_LONG);
        exitToast = Toast.makeText(this, "Press Back again to exit", Toast.LENGTH_SHORT);

        EMAIL = findViewById(R.id.email);
        PASSWORD = findViewById(R.id.password);
        TOGGLE_PASSWORD = findViewById(R.id.togglePassword);
        LOGIN = findViewById(R.id.loginButton);
        REGISTER = findViewById(R.id.register);
        REMEMBER_ME = findViewById(R.id.rememberMe);

        DB_HELPER = new DBHelper(this);
        DB_CONNECTION = DB_HELPER.getReadableDatabase();

        //// Shared Preference to check if any users is logged in
        sharedPreferences = getSharedPreferences("userData", Activity.MODE_PRIVATE);
        userId = sharedPreferences.getInt("userId", -1);
        sharedPreferenceEditor = sharedPreferences.edit();


        //// Check if user is logged in
        //// if userId == -1, no users is logged in. else send that id to next activity
        if (userId != -1) {

            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("userId", userId);
            startActivity(i);

        }

        //// If User Email & Password exists in Shared Preference
        //// Set those values to the input fields in Login Activity
        String userEmail = sharedPreferences.getString("userEmail", "");
        String userPassword = sharedPreferences.getString("userPassword", "");
        if( ! userEmail.equals("") ){
            EMAIL.setText(userEmail);
            PASSWORD.setText(userPassword);
        }

        suggestionConnection = DB_HELPER.getReadableDatabase();
        suggestionCursor = suggestionConnection.query(DBHelper.TABLE_NAME, new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_EMAIL},
                null, null, null, null, null);
        suggestionCursorAdapter = new SimpleCursorAdapter(this, R.layout.suggestion_list_view, suggestionCursor,
                new String[]{DBHelper.COLUMN_EMAIL}, new int[]{R.id.suggestion}, 0);


        //// cursor to String conversion - On Click Conversion
        ((SimpleCursorAdapter) suggestionCursorAdapter).setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                return cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_EMAIL));
            }
        });


        //// Filter List - Change Cursor according to input (constraint)
        suggestionCursorAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                suggestionCursor = suggestionConnection.query(DBHelper.TABLE_NAME, new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_EMAIL},
                        DBHelper.COLUMN_EMAIL + " like \"" + constraint + "%\"", null, null, null, DBHelper.COLUMN_EMAIL);
                return suggestionCursor;
            }
        });


        EMAIL.setAdapter(suggestionCursorAdapter);
        EMAIL.setThreshold(1);

    }


    public void onPasswordToggle(View v) {

        if (passwordVisible) {
            passwordVisible = false;
            TOGGLE_PASSWORD.setImageResource(R.drawable.make_password_visible);
            PASSWORD.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            passwordVisible = true;
            TOGGLE_PASSWORD.setImageResource(R.drawable.make_password_invisible);
            PASSWORD.setInputType(InputType.TYPE_CLASS_TEXT);
        }

    }


    public void onLogin(View v) {

        //// Authenticate
        String condition = DBHelper.COLUMN_EMAIL + " = ? and " + DBHelper.COLUMN_PASSWORD + " = ?";
        CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME, new String[]{DBHelper.COLUMN__ID},
                condition, new String[]{EMAIL.getText().toString(), PASSWORD.getText().toString()},
                null, null, null);


        //// If Valid User
        if (CURSOR.moveToFirst()) {

            //// If user is valid, & wants to be remembered, save their id, email & password in shared preference
            //// else set email & password as empty string
            if (REMEMBER_ME.isChecked()) {
                sharedPreferenceEditor.putInt("userId", CURSOR.getInt(CURSOR.getColumnIndex(DBHelper.COLUMN__ID)));
                sharedPreferenceEditor.putString("userEmail", EMAIL.getText().toString());
                sharedPreferenceEditor.putString("userPassword", PASSWORD.getText().toString());
                sharedPreferenceEditor.commit();
            }
            else {
                sharedPreferenceEditor.putString("userEmail", "");
                sharedPreferenceEditor.putString("userPassword", "");
                sharedPreferenceEditor.apply();
            }

            PASSWORD.setText("");
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("userId", CURSOR.getInt(CURSOR.getColumnIndex(DBHelper.COLUMN__ID)));
            startActivity(i);

        } else {
            invalidUserToast.show();
        }

        CURSOR.close();

    }

    public void onRegister(View v) {

        Intent i = new Intent(this, RegisterActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        invalidUserToast.cancel();
        exitToast.cancel();
        DB_CONNECTION.close();
        suggestionCursor.close();
        suggestionConnection.close();

    }

    @Override
    public void onBackPressed() {

        if (backPressed) {
            super.onBackPressed();
        }

        backPressed = true;
        exitToast.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                backPressed = false;
            }
        }, 2000);
    }

}
