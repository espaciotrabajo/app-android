package com.example.login;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateFragment extends Fragment {


    //// Widgets Variable
    public EditText FNAME;
    public EditText LNAME;
    public EditText EMAIL;
    public EditText PASSWORD;
    public EditText CONFIRM_PASSWORD;
    public ImageView TOGGLE_PASSWORD;
    public ImageView TOGGLE_CONFIRM_PASSWORD;
    public Button UPDATE;


    //// Database Variables
    public DBHelper DB_HELPER;
    public SQLiteDatabase DB_CONNECTION;
    public Cursor CURSOR;


    //// Other Variables
    public Context context;
    public Toast emailExistsToast;
    public int userId;

    public UpdateFragment() {
        // Required empty public constructor
    }

    public void setUserId(int id){
        userId = id;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    //// Toggle Password and Confirm Password
    public class PasswordToggleListener implements View.OnClickListener {

        private EditText PASSWORD;
        private ImageView TOGGLE_BUTTON;

        private boolean passwordVisible = false;

        PasswordToggleListener(EditText pass, ImageView toggle) {

            PASSWORD = pass;
            TOGGLE_BUTTON = toggle;

        }

        public void onClick(View v) {
            if (passwordVisible) {
                TOGGLE_BUTTON.setImageResource(R.drawable.make_password_visible);
                PASSWORD.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                passwordVisible = false;
            } else {
                TOGGLE_BUTTON.setImageResource(R.drawable.make_password_invisible);
                PASSWORD.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                passwordVisible = true;
            }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_update, container, false);


        //// Set Static variables to respective views
        FNAME = v.findViewById(R.id.fname);
        LNAME = v.findViewById(R.id.lname);
        EMAIL = v.findViewById(R.id.email);
        PASSWORD = v.findViewById(R.id.password);
        CONFIRM_PASSWORD = v.findViewById(R.id.confirmPassowrd);
        TOGGLE_PASSWORD = v.findViewById(R.id.togglePassword);
        TOGGLE_CONFIRM_PASSWORD = v.findViewById(R.id.toggleConfirmPassword);
        UPDATE = v.findViewById(R.id.update);

        emailExistsToast = Toast.makeText(context, "Given Email ID is already registered by another user", Toast.LENGTH_LONG);
        DB_HELPER = new DBHelper(context);
        DB_CONNECTION = DB_HELPER.getWritableDatabase();
        CURSOR = DB_CONNECTION.query(DBHelper.TABLE_NAME,
                new String[]{DBHelper.COLUMN__ID, DBHelper.COLUMN_FIRST_NAME, DBHelper.COLUMN_LAST_NAME, DBHelper.COLUMN_EMAIL, DBHelper.COLUMN_PASSWORD},
                DBHelper.COLUMN__ID + " = ?", new String[]{String.valueOf(userId)},
                null, null, null);
        CURSOR.moveToFirst();

        FNAME.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_FIRST_NAME)));
        LNAME.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_LAST_NAME)));
        EMAIL.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_EMAIL)));
        PASSWORD.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_PASSWORD)));
        CONFIRM_PASSWORD.setText(CURSOR.getString(CURSOR.getColumnIndex(DBHelper.COLUMN_PASSWORD)));

        //// Toggle Password
        TOGGLE_PASSWORD.setOnClickListener(new PasswordToggleListener(PASSWORD, TOGGLE_PASSWORD));

        //// Toggle Confirm Password
        TOGGLE_CONFIRM_PASSWORD.setOnClickListener(new PasswordToggleListener(CONFIRM_PASSWORD, TOGGLE_CONFIRM_PASSWORD));


        //// Validate Input Fields
        UPDATE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean valid = true;


                //// Check First Name
                if (!FNAME.getText().toString().matches("[a-zA-Z]+")) {
                    FNAME.setError("Invalid. First Name can contain only A-Z a-z");
                    valid = false;
                }


                //// Check Last Name
                if (!LNAME.getText().toString().matches("[a-zA-Z]+")) {
                    LNAME.setError("Invalid. Last Name can contain only A-Z a-z");
                    valid = false;
                }


                //// Check Email
                if (!EMAIL.getText().toString().matches("[a-zA-Z0-9.]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+")) {
                    EMAIL.setError("Invalid Email");
                    valid = false;
                }
                Cursor checkEmail = DB_CONNECTION.query(DBHelper.TABLE_NAME, new String[]{DBHelper.COLUMN_EMAIL},
                        DBHelper.COLUMN_EMAIL + " = ? and NOT " + DBHelper.COLUMN__ID + " = ?",
                        new String[]{EMAIL.getText().toString(), String.valueOf(userId)},
                        null, null, null);
                if (checkEmail.moveToFirst()) {
                    EMAIL.setError("Invalid Email");
                    emailExistsToast.show();
                    valid = false;
                }

                //// Check Password
                if (PASSWORD.getText().toString().length() < 8 || PASSWORD.getText().toString().length() > 15) {
                    PASSWORD.setError("Password length should be between 8 - 15 characters");
                    valid = false;
                }


                //// Check Confirm Password
                if (!CONFIRM_PASSWORD.getText().toString().equals(PASSWORD.getText().toString())) {
                    CONFIRM_PASSWORD.setError("Password does not match.");
                    valid = false;
                }


                //// If All input fields are valid
                if (valid) {

                    try {

                        //// Update Data into Database
                        int result = DB_HELPER.updateData(DB_CONNECTION, userId, FNAME.getText().toString(), LNAME.getText().toString(), EMAIL.getText().toString(), PASSWORD.getText().toString());
                        Toast.makeText(context, "Updated Data", Toast.LENGTH_SHORT).show();
                        getFragmentManager().popBackStack();

                    } catch (SQLiteConstraintException e) {
                        emailExistsToast.show();
                    }
                }

            }
        });

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CURSOR.close();
        emailExistsToast.cancel();
    }



}
